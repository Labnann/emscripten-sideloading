#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <emscripten.h>
#include <emscripten/fetch.h>

int run_loaded_file();

int saveToFile(const char* file_name, const unsigned char* data, size_t size){
	FILE *fp = fopen(file_name, "wb");

	if (fp) {
		fwrite(data, sizeof(unsigned char),size, fp);
		fclose(fp);
                printf("saved\n");
	}
	else return -1;

	return 0;

}
void downloadSucceeded(emscripten_fetch_t *fetch) {
    printf("Download succeeded!\n");

    unsigned char* data = fetch->data;
    size_t size = fetch->numBytes;

    saveToFile("hello.so", data, size);

    emscripten_fetch_close(fetch);
    run_loaded_file();
}

void downloadFailed(emscripten_fetch_t *fetch) {
    printf("Download failed!\n");

    emscripten_fetch_close(fetch); // Cleanup the fetch request
}

void download(const char* url) {
    emscripten_fetch_attr_t attr;
    emscripten_fetch_attr_init(&attr);
    strcpy(attr.requestMethod, "GET");
    attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY;

    attr.onsuccess = downloadSucceeded;
    attr.onerror = downloadFailed;

    emscripten_fetch_t* fetch = emscripten_fetch(&attr, url);
}


int run_loaded_file(){
    void *handle;
    void (*hello_world)();


    
    handle = dlopen("./hello.so", RTLD_LAZY);
    if (!handle) {
        fprintf(stderr, "Error: %s\n", dlerror());
        return 1;
    }

    hello_world = dlsym(handle, "hello_world");
    if (dlerror() != NULL) {
        fprintf(stderr, "Error: %s\n", dlerror());
        dlclose(handle);
        return 1;
    }

    hello_world();

    dlclose(handle);

    return 0;

}


int main() {
    download("hello.so");

    return 0;
}

