=======================
Emscripten SideLoading
=======================

A simple Hello World Project to understand Emscripten SideLoading
=================================================================

Guidelines:
-----------
For browser demonstration use::

    $ sh ./build-emscripten-browser.sh

The files necessary to run in browser will be in build-emscripten-browser folder.
